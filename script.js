/* STICKY NAVIGATION */

let header = document.querySelector(".navigation");

// position de la barre de navigation
let headerPosition = header.offsetTop;

// fonction pour ajouter la classe sticky quand on scroll
function stickyHeader() {
  if (window.pageYOffset > headerPosition) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
};

window.onscroll = function() {stickyHeader()};

/* MENU */

let bars = document.querySelector(".toggle-menu");
let menu = document.querySelector(".nav-container");


bars.addEventListener("click", function() {
  menu.classList.toggle("mobile-menu");
});


/* SEARCH BAR */

let searchNav = document.getElementById("search-nav");
let heart = document.querySelector(".heart");
let nav = document.querySelector(".navigation nav");

// search remplace le menu quand on clique
searchNav.addEventListener("click", function(event) {
    event.stopPropagation();
    searchNav.classList.add("search--full");
    searchNav.classList.remove("search");
    heart.style.display = "none";
    nav.style.display = "none";
});

let closeIcon = document.querySelector(".navigation .close-icon");

// l'icone en croix ferme le search
closeIcon.addEventListener("click", function(event) {
  event.stopPropagation();
  searchNav.classList.remove("search--full");
  searchNav.classList.add("search");
  heart.style.display = "block";
  nav.style.display = "block";
});


